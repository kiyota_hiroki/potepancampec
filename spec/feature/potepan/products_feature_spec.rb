RSpec.feature "Products", type: :feature do
  describe "Products" do
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let!(:image) { create(:image) }

    before do
      product.images << image
      visit potepan_product_path(product.id)
    end

    context "商品詳細画面のテスト" do
      it "「一覧ページへ戻る」をクリックしたら、前のページに戻る" do
        click_on "一覧ページへ戻る"
        visit potepan_category_path(product.taxons.first.id)
        expect(page).to have_content "商品カテゴリー"
      end
    end
  end
end
