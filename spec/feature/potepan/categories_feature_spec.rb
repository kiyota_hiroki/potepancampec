RSpec.feature "Categories", type: :feature do
  describe "Categories" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:image) { create(:image) }

    before do
      product.images << image
      visit potepan_category_path(taxon.id)
    end

    describe "categoryページ" do
      it "カテゴリーが表示されている" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon.name
      end

      it "商品名をクリックすると商品詳細ページへ遷移する" do
        click_on product.name
        expect(current_path).to eq potepan_product_path(product.id)
      end

      it "商品価格をクリックすると商品詳細ページへ遷移する" do
        click_on "#{product.display_price}"
        expect(current_path).to eq potepan_product_path(product.id)
      end

      it "HOMEリンクをクリックするとHOME画面に遷移する" do
        click_on "HOME", match: :first
        expect(current_path).to eq potepan_path
      end

      it "ブランドロゴをクリックするとHOME画面に遷移する" do
        click_on "ブランドロゴ"
        expect(current_path).to eq potepan_path
      end
    end
  end
end
