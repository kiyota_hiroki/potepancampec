RSpec.describe "ApplicationHelper" do
  describe "#full_title" do
    include ApplicationHelper
    it "full_titleメソッドに商品名が渡されること" do
      expect(full_title("RUBY ON RAILS BAG")).to eq "RUBY ON RAILS BAG - #{ApplicationHelper::BASE_TITLE}" # rubocop:disable Metrics/LineLength
    end

    it "full_titleメソッドの引数にnilが渡されること" do
      expect(full_title(nil)).to eq ApplicationHelper::BASE_TITLE
    end

    it "full_titleメソッドの引数に空が渡されること" do
      expect(full_title("")).to eq ApplicationHelper::BASE_TITLE
    end

    it "full_titleメソッドにカテゴリー名が渡されること" do
      expect(full_title("Categories")).to eq "Categories - #{ApplicationHelper::BASE_TITLE}"
    end
  end
end
