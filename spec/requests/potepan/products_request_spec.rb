RSpec.describe "Potepan::Products", type: :request do
  describe "GET sample#index" do
    before do
      get potepan_path
    end

    it { expect(response).to have_http_status 200 }
  end

  describe "GET products#show" do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }

    before do
      get potepan_product_path(product.id)
    end

    it { expect(response).to have_http_status 200 }

    it "商品名が適切に表示されていること" do
      expect(response.body).to include product.name
    end

    it "価格が適切に表示されていること" do
      expect(response.body).to include "#{product.display_price}"
    end

    it "商品説明が適切に表示されていること" do
      expect(response.body).to include product.description
    end
  end
end
