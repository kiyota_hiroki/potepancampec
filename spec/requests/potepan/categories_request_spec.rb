RSpec.describe "Potepan::Categories", type: :request do
  describe "GET categoires#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:image) { create(:image) }

    before do
      product.images << image
      get potepan_category_path(taxon.id)
    end

    it { expect(response).to have_http_status 200 }

    context "商品カテゴリー" do
      it "taxonomyの名前が表示されていること" do
        expect(response.body).to include taxonomy.name
      end

      it "taxonの名前が表示されていること" do
        expect(response.body).to include taxon.name
      end
    end

    context "商品一覧" do
      it "商品の名前が表示されていること" do
        expect(response.body).to include product.name
      end

      it "商品の値段が表示されていること" do
        expect(response.body).to include "#{product.display_price}"
      end
    end
  end
end
